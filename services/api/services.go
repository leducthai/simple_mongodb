package api

import (
	"context"

	"gitlab.com/leducthai/simple_mongodb/services/db"
)

type ServicesInter interface {
	UserService() db.User
	AccountService() db.Account
}
type Services struct {
	User    db.User
	Account db.Account
}

var instanceServices *Services

func NewServices(dm db.DataManager) ServicesInter {
	if instanceServices == nil {
		instanceServices = &Services{
			User:    dm.UserService(),
			Account: dm.Account(),
		}
	}
	return *instanceServices
}

func (s Services) UserService() db.User {
	return s.User
}

func (s Services) AccountService() db.Account {
	return s.Account
}

func GetIsInBlackList(token string) (bool, error) {
	re, err := instanceServices.AccountService().IsInBlackList(context.Background(), token)
	if err != nil {
		return false, err
	}
	return re, nil
}
