package db

import (
	"context"
)

// mothods which are provided by account service
type Account interface {
	Login(context.Context, LoginReq) (LoginReply, error)
	CreateAccount(context.Context, CreateAccountRequset) error
	Logout(context.Context, LogoutRequest) error
	ResetToken(context.Context, LogoutRequest) (LoginReply, error)
	IsInBlackList(context.Context, string) (bool, error)
}

// Requests and replies form

type LoginReq struct {
	Email    string `bson:"email" json:"email"`
	Password string `bson:"password" json:"password"`
}

type LoginReply struct {
	Token string `bson:"token" json:"token"`
}

type AccountReply struct {
	Email      string  `bson:"email"`
	Password   []byte  `bson:"password"`
	Permisions []int32 `bson:"permisions"`
}

type JwtClaims struct {
	Email      string
	Permisions []int32
}

type CreateAccountRequset struct {
	Email      string  `bson:"email" json:"email"`
	Password   string  `bson:"password" json:"password"`
	Permisions []int32 `bson:"permisions" json:"permisions"`
}

type AccountMongo struct {
	Email      string  `bson:"email"`
	Password   []byte  `bson:"password"`
	Permisions []int32 `bson:"permisions"`
}

type LogoutRequest struct {
	Token string `bson:"token" json:"token"`
}
