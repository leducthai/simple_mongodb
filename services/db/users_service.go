package db

import (
	"context"
)

// Methods which are provide by user service
type User interface {
	GetUser(context.Context, string, bool) (interface{}, error)
	CreateUser(context.Context, CreateUserRequset) error
	DeleteUser(context.Context, string) (CommonUpsertReply, error)
	UpdateUser(context.Context, string, UpsertUsertRequest) (CommonUpsertReply, error)
}

// Request and reply forms
type GetUserReply struct {
	Name string `bson:"name" json:"name"`
	Age  int    `bson:"age" json:"age"`
	Bio  string `bson:"bio" json:"bio"`
}

type GetUserReplyAdmin struct {
	Name string `bson:"name" json:"name"`
	Bio  string `bson:"bio" json:"bio"`
}

type UpsertUsertRequest struct {
	Name string `bson:"name" json:"name"`
	Age  int    `bson:"age" json:"age"`
	Bio  string `bson:"bio" json:"bio"`
}

type CreateUserRequset struct {
	Email string `bson:"email" json:"email"`
	Name  string `bson:"name" json:"name"`
	Age   int    `bson:"age" json:"age"`
	Bio   string `bson:"bio" json:"bio"`
}

type CommonUpsertReply struct {
	RowAffected int64
}
