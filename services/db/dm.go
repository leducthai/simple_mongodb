package db

type DataManager interface {
	Close()
	UserService() User
	Account() Account
}
