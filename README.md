# simple_mongodb

## Data-flow diagram
![Data-flow](images/work-flow.png)
### explain temporary storage
- when users call to get data from mongodb base if it is already in redis they don't need to connect to database, if it is not in redis they will connect to database and store a coppy in redis for later use. 
- if the data in redis hit the maximum it will use LRU(least rerently use) method to pop out data
## Project Structure
### mains structure

- [config](config)
- [images](images)
- [mongo](mongo)
- [server](server)
- [services](services)
- [testservices](testservices)
- [utils](utils)

### detail structure
```bash
/simple_mongodb/
├──config
│   └──redis.conf
├──images
├──mongo
│   ├──mocks
│   │   ├──mockgen
│   │   │   └──mock_gen.go
│   │   └──mockoperators
│   │       ├──Account.go
│   │       ├──ServicesInter.go
│   │       └──User.go
│   └──operators
│       ├──auth
│       │   ├──impl.go
│       │   └──jwt.go
│       ├──users
│       │  └──impl.go
│       └──impl.go       
├──server
│   ├──config
│   │   └──configs.go
│   ├──handler
│   │   ├──auth
│   │   │   ├──respone.go
│   │   │   └──account_test.go
│   │   └──users
│   │       ├──respone.go
│   │       └──users_test.go
│   ├──middleware
│   │   ├──authenticate.go
│   │   └──logging.go
│   ├──response
│   │   └──response.go
│   ├──routing
│   │   ├──auth.go
│   │   └──users.go
│   ├──routing.go
│   └──server.go
├──service
│   ├──api
│   │   └──services.go
│   └──db
│       ├──account_service.go
│       ├──dm.go
│       └──users_service.go
├──testservices
│   ├──auth
│   │   └──impl_test.go
│   └──users
│       └──impl_test.go
├──utils
│   ├──handler_test
│   │   └──test.go
│   ├──perms
│   │   ├──check.go
│   │   ├──perm.yaml
│   │   ├──perms_gen.go
│   │   ├──perms.pb.go
│   │   └──perms.proto
│   └──testservices
│       ├──setup.go
│       └──suite.go
├──.dockerignore
├──.env
├──.gitignore
├──docker-compose.yml
├──Dockerfile
├──go.mod
├──go.sum
├──main.go
└──README.md
```

## DEMO
- **First you must clone the project**  
make sure you have Git installed in your computer
```bash
    git clone https://gitlab.com/leducthai/simple_mongodb.git
```
- **To run demo of this project using Docker**
>- first make sure you have install Docker Destop if you haven't follow these urls  
[docker destop for windowns](https://docs.docker.com/desktop/install/windows-install/)  
[docker destop for mac](https://docs.docker.com/desktop/install/mac-install/)  
[docker destop for linux](https://docs.docker.com/desktop/install/linux-install/)

>- at the main directory /simple-mongodb/ run this code. This code will takes environment variables in .env file to run the demo server you can edit the .env file to set your desired environment
```bash
    docker-compose down && docker-compose up
```
### Optional editing 
*note: rerun demo after editing, if you do not edit demo should run just fine
- **Edit .env file** 
>- [go to .env file](./.env) (/simple_mongo/.env)
```bash
    # THE FORMAT OF .EMV FILE WILL BE AS FOLLOW

    # these refer to your init user, and password when you run mongodb server (this is used to connect to your mongo server)
    MONGO_INIT_USER=xxxxxx
    MONGO_INIT_PASSWORD=xxxxxxx
    # this refer to secret key to generate and verify token key
    CUSTOM_SECRET_KEY=secret_key_should_be_hind
    # this must follow the password in config/redis.cof
    CUSTOM_REDIS_PASSWORD=edit_your_own_password_for_redis
```
- **Edit redis.conf**
>- [go to redis.conf](./config/redis.conf) (/simple_mongodb/config/redis.conf)  
```bash
    # this set password for your redis server (onece you edited this you must edit CUSTOM_REDIS_PASSWORD in .env file )
    requirepass edit_your_own_password_for_redis
    # this refers to permitted IP to connect to Redis (0.0.0.0 means no constraint)
    bind 0.0.0.0
```
- **Edit perm**
>- [go to perm,yaml file](./utils/perms/perm.yaml) (/simple_mongodb/utils/perms/perm.yaml)
```bash
    # FORMAT OF PERM.YAML FILE
    inituser:
        admin:
            # edit your desire admin account 
            # email|password
            admin_role@gmail.com|admin_password
        dev: 
            # edit your desire dev account 
            # email|password
            dev_role@gmail.com|dev_password  
    permissions:
        # this indicate which role can perform which function
        # edit the permissions for each function in the form of:    
        #   FUNC_NAME:      (this must follow FuncNames the perms.proto file)
        #       - role1     (role must follow Roles in perms.proro file )
        #       - role2
        GetUser:
            - Admin
            - User
        GetUserID:
            - Admin
        CreateUser:
            - Admin 
            - User
        DeleteUser:
            - Admin
        UpdateUser: 
            - User
            - Admin
            - Dev
        Logout:
            - Admin
            - User
            - Dev
        ReFresh:
            - User
            - Admin
            - Dev
        CreateAccount:
            - Admin
```
### APIs test 
- default accounts
```bash
    admin:
        {
            "email": "admin_role@gmail.com",
            "password": "admin_password"
        }
    dev:
        {
            "email": "dev_role@gmail.com",
            "password": "dev_password"
        }
```
- Login
```bash 
    POST http://localhost:7171/api/accounts/login
    Authorization_require: No
    Example:
    Body (JSON):
    {
        "email": "dev_role@gmail.com",
        "password": "dev_password"
    }
    Reply:
    {
        "token": "xxxxxx-xxxxxx"
    }

```
- Logout
```bash
    POST http://localhost:7171/api/accounts/logout
    Authorization_require: Yes
    Example:
    Body (JSON): not require
    Reply:
    {
        "detail": "ok or error_detail"
    }
```
- Refresh 
```bash
    POST http://localhost:7171/api/accounts/refresh
    Authorization_require: Yes
    Example:
    Body (JSON): not require
    Reply:
        error_case:
        {
            "detail": "error detail"
        }
        good_case:
        {
            "token": "new_token_with_reset_expire_time"
        }
```
- Create account
```bash
    POST http://localhost:7171/api/accounts
    Authorization_require: Yes
    Example:
    Body (JSON): 
    {
        "name": "put_name_in_here",
        "age": 0,
        "bio": "put_the_bio_in_here"
    }
    Reply:
    {
        "detail": "ok or error_detail"
    }
```
- Retrieve user
```bash
    case 1
    GET http://localhost:7171/api/users 
    Authorization_require: Yes
    Example:
    Body (JSON): not require
    Reply:
        error_case:
        {
            "detail": "error detail"
        }
        good_case:
        {
            "name": "name_of_this_account",
            "age": 0,
            "bio": "bio_of_this_account"
        }
    case 2 (Admin only) # admin can see profile of anyone but not their age "desire_email" is the email of anyone
    GET http://localhost:7171/api/users?email=desire_email  
    Authorization_require: Yes
    Example:
    Body (JSON): not require
    Reply:
        error_case:
        {
            "detail": "error detail"
        }
        good_case:
        {
            "name": "name_of_desire_account",
            "bio": "bio_of_desire_account"
        }
```
- Create user
```bash
    POST http://localhost:7171/api/users
    Authorization_require: Yes
    Example:
    Body (JSON): 
    {
        "name": "put_the_name_you_want_in_here",
        "age": 0,
        "bio": "your_bio"
    }
    Reply:
    {
        "detail": "ok or error_detail"
    }
```
- Update user
```bash
    PUT http://localhost:7171/api/users
    Authorization_require: Yes
    Example:
    Body (JSON):
    {
        "name": "new_name",
        "age": 0,
        "bio": "new_bio"
    }
    Reply:
    {
        "detail": "ok or error_detail"
    }
```
- Delete users
```bash
    DELETE http://localhost:7171/api/users
    Authorization_require: Yes
    Example:
    Body (JSON): not require
    Reply: 
    {
        "detail": "ok or error_detail"
    }
```
