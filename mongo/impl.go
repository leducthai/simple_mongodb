package mongo

import (
	"context"
	"fmt"
	"log"

	"github.com/redis/go-redis/v9"
	"gitlab.com/leducthai/simple_mongodb/mongo/operations/auth"
	"gitlab.com/leducthai/simple_mongodb/mongo/operations/users"
	"gitlab.com/leducthai/simple_mongodb/services/db"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConfig struct {
	// Mongo server
	Address  string
	Port     int
	Username string
	Password string

	// Redis server
	RedisConnect string
	RedisPass    string
}

type MongoManager struct {
	Client    *mongo.Client
	MDatabase *mongo.Database
	Red       *redis.Client
}

func New(mc MongoConfig, dbname string, reidsdb int) db.DataManager {

	rdb := NewRedis(mc, reidsdb)
	client, database := NewDB(mc, dbname)

	return &MongoManager{
		Client:    client,
		MDatabase: database,
		Red:       rdb,
	}
}

func NewDB(mc MongoConfig, name string) (*mongo.Client, *mongo.Database) {
	// MongoDB connection string with credentials
	connectionURI := fmt.Sprintf("mongodb://%s:%s@%s:%d", mc.Username, mc.Password, mc.Address, mc.Port)

	// Create a MongoDB client
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(connectionURI))
	if err != nil {
		log.Fatalf("Failed to connect to MongoDB: %v", err)
		return nil, nil
	}

	// Ping the MongoDB server to check the connection
	err = client.Ping(context.Background(), nil)
	if err != nil {
		log.Fatalf("Failed to ping MongoDB: %v", err)
		return nil, nil
	}

	database := client.Database(name)

	return client, database
}

func NewRedis(mc MongoConfig, rdb int) *redis.Client {
	r := redis.NewClient(&redis.Options{
		Addr:     mc.RedisConnect,
		Password: mc.RedisPass,
		DB:       rdb,
	})

	// Ping Redis to check the connection
	_, err := r.Ping(context.Background()).Result()
	if err != nil {
		log.Println(err)
	}

	return r
}

func (mm *MongoManager) Close() {
	client := mm.Client
	// Close the MongoDB client when you're done
	err := client.Disconnect(context.Background())
	if err != nil {
		log.Fatalf("Failed to disconnect from MongoDB: %v", err)
	}
	log.Printf("mongodb is closed")
}

func (dm MongoManager) UserService() db.User {
	return users.NewService(dm.MDatabase, dm.Red)
}

func (dm MongoManager) Account() db.Account {
	return auth.NewService(dm.MDatabase)
}
