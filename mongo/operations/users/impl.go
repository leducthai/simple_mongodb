package users

import (
	"context"
	"fmt"
	"log"

	"github.com/redis/go-redis/v9"
	"gitlab.com/leducthai/simple_mongodb/services/db"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	redisLimit = 100
)

type Service struct {
	Collection *mongo.Collection
	Red        *redis.Client
}

func NewService(dm *mongo.Database, r *redis.Client) db.User {
	col := dm.Collection("users")

	// Create a unique index on the "email" field if it doesn't already exist
	indexModel := mongo.IndexModel{
		Keys:    bson.M{"email": 1},
		Options: options.Index().SetUnique(true),
	}

	// Check if the index already exists
	cursor, err := col.Indexes().List(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	for cursor.Next(context.Background()) {
		var index bson.M
		if err := cursor.Decode(&index); err != nil {
			log.Println(err)
		}

		if index["email"] == "email_1" {
			log.Println("Unique index on 'name' already exists.")
			return Service{
				Collection: col,
				Red:        r,
			}
		}
	}

	// Create the unique index on 'email'
	_, err = col.Indexes().CreateOne(context.Background(), indexModel)
	if err != nil {
		log.Fatal(err)
	}

	return Service{
		Collection: col,
		Red:        r,
	}
}

func (s Service) GetUser(ctx context.Context, email string, isOwner bool) (interface{}, error) {
	filter := bson.M{"email": email}

	ans, err := s.getFromRedis(ctx, email, isOwner)
	if err != nil {
		log.Printf("failed to get from redis err: %v", err)
	} else {
		return ans, nil
	}

	if isOwner {
		user := db.GetUserReply{}
		err := s.Collection.FindOne(context.Background(), filter).Decode(&user)
		if err != nil {
			if err == mongo.ErrNoDocuments {
				return db.GetUserReply{}, fmt.Errorf("no matching document found")
			}
			return db.GetUserReply{}, fmt.Errorf("failed to execute find one query: %v", err)
		}

		err = s.createFromRedis(ctx, db.CreateUserRequset{
			Email: email,
			Name:  user.Name,
			Age:   user.Age,
			Bio:   user.Bio,
		})
		if err != nil {
			log.Printf("failed to add to redis err: %v", err)
		}

		return user, nil
	} else {
		userNotOwner := db.GetUserReplyAdmin{}
		err := s.Collection.FindOne(context.Background(), filter).Decode(&userNotOwner)
		if err != nil {
			if err == mongo.ErrNoDocuments {
				return db.GetUserReply{}, fmt.Errorf("no matching document found")
			}
			return db.GetUserReply{}, fmt.Errorf("failed to execute find one query: %v", err)
		}
		return userNotOwner, nil
	}
}

func (s Service) CreateUser(ctx context.Context, req db.CreateUserRequset) error {
	_, err := s.Collection.InsertOne(ctx, req)
	if err != nil {
		if mongo.IsDuplicateKeyError(err) {
			return fmt.Errorf("duplicate key: %v", err)
		}
		return err
	}

	err = s.createFromRedis(ctx, req)
	if err != nil {
		log.Printf("failed to add to redis err: %v", err)
	}

	return nil
}

func (s Service) DeleteUser(ctx context.Context, email string) (db.CommonUpsertReply, error) {
	filter := bson.M{"email": email}
	deleteResult, err := s.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return db.CommonUpsertReply{}, err
	}
	if deleteResult.DeletedCount == int64(0) {
		return db.CommonUpsertReply{}, fmt.Errorf("not found")
	}

	err = s.deleteFromRedis(ctx, email)
	if err != nil {
		return db.CommonUpsertReply{}, err
	}

	return db.CommonUpsertReply{RowAffected: deleteResult.DeletedCount}, nil
}

func (s Service) UpdateUser(ctx context.Context, email string, req db.UpsertUsertRequest) (db.CommonUpsertReply, error) {
	filter := bson.M{"email": email}
	update := bson.M{"$set": req}
	updateResult, err := s.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return db.CommonUpsertReply{}, err
	}
	if updateResult.MatchedCount == int64(0) {
		return db.CommonUpsertReply{}, fmt.Errorf("not found")
	}

	err = s.createFromRedis(ctx, db.CreateUserRequset{
		Email: email,
		Name:  req.Name,
		Age:   req.Age,
		Bio:   req.Bio,
	})
	if err != nil {
		return db.CommonUpsertReply{}, fmt.Errorf("update redis failed err: %v", err)
	}

	return db.CommonUpsertReply{RowAffected: updateResult.MatchedCount}, nil
}
