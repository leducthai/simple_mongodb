package users

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/leducthai/simple_mongodb/services/db"
)

func (s Service) createFromRedis(ctx context.Context, req db.CreateUserRequset) error {
	// Check if the limit is reached
	count, err := s.Red.DBSize(ctx).Result()
	if err != nil {
		return err
	}
	// prepare data
	email := req.Email
	data, err := json.Marshal(db.GetUserReply{
		Name: req.Name,
		Age:  req.Age,
		Bio:  req.Bio,
	})
	if err != nil {
		return err
	}

	if count >= int64(redisLimit) {
		// Evict the least recently used (LRU) key
		evictedKey, err := s.Red.LPop(ctx, "recently_used_keys").Result()
		if err != nil {
			return err
		}

		err = s.deleteFromRedis(ctx, evictedKey)
		if err != nil {
			return err
		}
	}
	// Add the new key-value pair
	err = s.Red.Set(ctx, email, string(data), 0).Err()
	if err != nil {
		return err
	}

	// Update the recently used keys list
	err = s.Red.RPush(ctx, "recently_used_keys", email).Err()
	if err != nil {
		return err
	}
	return nil
}

func (s Service) deleteFromRedis(ctx context.Context, email string) error {
	err := s.Red.Del(ctx, email).Err()
	if err != nil {
		return fmt.Errorf("redis delete err: %v", err)
	}
	lenRA, err := s.Red.LLen(ctx, "recently_used_keys").Result()
	if err != nil {
		return fmt.Errorf("redis delete err: %v", err)
	}
	for i := lenRA; i > 0; i-- {
		leftMostValue := s.Red.LIndex(ctx, "recently_used_keys", 0).String()
		existed, err := s.Red.Exists(ctx, leftMostValue).Result()
		if err != nil {
			return fmt.Errorf("redis delete err: %v", err)
		}
		if existed > 0 {
			_, err := s.Red.LPop(ctx, "recently_used_keys").Result()
			if err != nil {
				return fmt.Errorf("redis delete err: %v", err)
			}
		}
	}
	return nil
}

func (s Service) getFromRedis(ctx context.Context, email string, owner bool) (interface{}, error) {
	exists, err := s.Red.Exists(ctx, email).Result()
	if err != nil {
		return db.GetUserReply{}, err
	}
	if exists != 1 {
		return db.GetUserReply{}, fmt.Errorf("not found")
	}

	jstr, err := s.Red.Get(ctx, email).Result()
	if err != nil {
		return db.GetUserReply{}, err
	}
	rt := db.GetUserReply{}

	err = json.Unmarshal([]byte(jstr), &rt)
	if err != nil {
		return db.GetUserReply{}, err
	}
	if !owner {
		return db.GetUserReplyAdmin{
			Name: rt.Name,
			Bio:  rt.Bio,
		}, nil
	}
	return rt, nil
}
