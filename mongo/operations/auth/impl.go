package auth

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/leducthai/simple_mongodb/services/db"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
)

type Service struct {
	Collection *mongo.Collection
	BlackList  *mongo.Collection
}

func NewService(dm *mongo.Database) db.Account {
	col := dm.Collection("accounts")
	bll := dm.Collection("blacklist")

	{ // add index to collection account
		// Create a unique index on the "email" field if it doesn't already exist
		indexModel := mongo.IndexModel{
			Keys:    bson.M{"email": 1},
			Options: options.Index().SetUnique(true),
		}

		// Check if the index already exists
		isExisted := false
		cursor, err := col.Indexes().List(context.Background())
		if err != nil {
			log.Fatal(err)
		}
		for cursor.Next(context.Background()) {
			var index bson.M
			if err := cursor.Decode(&index); err != nil {
				log.Println(err)
			}

			if index["name"] == "email_1" {
				log.Println("Unique index on 'email' already exists.")
				isExisted = true
			}
		}

		// Create the unique index on 'email'
		if !isExisted {
			_, err = col.Indexes().CreateOne(context.Background(), indexModel)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
	{ // add index to collection blacklist
		// Create a expire time index on the "expireat" field if it doesn't already exist
		indexModel := mongo.IndexModel{
			Keys:    bson.M{"expireat": 1},
			Options: options.Index().SetExpireAfterSeconds(0),
		}

		// Check if the index already exists
		isExisted := false
		cursor, err := bll.Indexes().List(context.Background())
		if err != nil {
			log.Fatal(err)
		}
		for cursor.Next(context.Background()) {
			var index bson.M
			if err := cursor.Decode(&index); err != nil {
				log.Println(err)
			}

			if index["name"] == "expireat_1" {
				log.Println("Unique index on 'expireat' already exists.")
				isExisted = true
			}
		}

		// Create the expier time index on 'expireat'
		if !isExisted {
			_, err = bll.Indexes().CreateOne(context.Background(), indexModel)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	return Service{
		Collection: col,
		BlackList:  bll,
	}
}

func (s Service) Login(ctx context.Context, req db.LoginReq) (db.LoginReply, error) {
	acc, err := s.getAccount(ctx, req.Email)
	if err != nil {
		return db.LoginReply{}, err
	}

	err = bcrypt.CompareHashAndPassword(acc.Password, []byte(req.Password))
	if err != nil {
		if err == bcrypt.ErrMismatchedHashAndPassword {
			return db.LoginReply{}, fmt.Errorf("wrong password or email")
		}
		return db.LoginReply{}, err
	}

	tkenKey, err := GenerateJwt(db.JwtClaims{
		Email:      acc.Email,
		Permisions: acc.Permisions,
	})
	if err != nil {
		return db.LoginReply{}, err
	}

	return db.LoginReply{Token: tkenKey}, nil
}

func (s Service) getAccount(ctx context.Context, email string) (db.AccountReply, error) {
	filter := bson.M{"email": email}
	var account db.AccountReply
	err := s.Collection.FindOne(ctx, filter).Decode(&account)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return db.AccountReply{}, fmt.Errorf("No matching document found.")
		}
		return db.AccountReply{}, fmt.Errorf("Failed to execute find one query: %v", err)
	}

	return account, nil
}

func (s Service) CreateAccount(ctx context.Context, req db.CreateAccountRequset) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	instance := db.AccountMongo{
		Email:      req.Email,
		Password:   hashedPassword,
		Permisions: req.Permisions,
	}
	_, insertErr := s.Collection.InsertOne(ctx, instance)
	if insertErr != nil {
		if mongo.IsDuplicateKeyError(insertErr) {
			return fmt.Errorf("email already existed: %v", insertErr)
		}
		return insertErr
	}
	return nil
}

func (s Service) Logout(ctx context.Context, req db.LogoutRequest) error {
	document := bson.M{
		"token":    req.Token,
		"expireat": time.Now().Add(time.Minute * 1), // Set expiration time 1 minute from now
	}

	_, err := s.BlackList.InsertOne(ctx, document)
	if err != nil {
		return err
	}
	return nil
}

func (s Service) ResetToken(ctx context.Context, req db.LogoutRequest) (db.LoginReply, error) {
	claims, err := VerifyToken(req.Token)
	if err != nil {
		return db.LoginReply{}, err
	}

	//  add curent key to blacklist
	s.Logout(ctx, req)

	token, err := GenerateJwt(db.JwtClaims{
		Email:      claims.Email,
		Permisions: claims.Permisions,
	})
	if err != nil {
		return db.LoginReply{}, err
	}

	return db.LoginReply{Token: token}, nil
}

func (s Service) IsInBlackList(ctx context.Context, tken string) (bool, error) {
	var instance = new(db.LoginReply)
	filter := bson.M{"token": tken}
	err := s.BlackList.FindOne(ctx, filter).Decode(&instance)
	if err != nil {
		if mongo.ErrNoDocuments == err {
			return false, nil
		}
		return true, err
	}
	if instance != nil {
		return true, nil
	}
	return false, nil
}
