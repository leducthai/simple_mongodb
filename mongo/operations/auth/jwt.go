package auth

import (
	"fmt"
	"os"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.com/leducthai/simple_mongodb/services/db"
)

var secret = os.Getenv("SECRET_KEY")

type CustomClaim struct {
	Email      string  `json:"email"`
	Permisions []int32 `json:"roles"`
	jwt.StandardClaims
}

// GenerateJwt generates jwt token with claim email and permisions
func GenerateJwt(cl db.JwtClaims) (string, error) {
	if secret == "" {
		return "", fmt.Errorf("secret key not found")
	}

	claims := CustomClaim{
		Email:      cl.Email,
		Permisions: cl.Permisions,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(10 * time.Minute).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// sign token with secret key
	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// VerifyToken verify if the token is legit
func VerifyToken(ts string) (CustomClaim, error) {
	if secret == "" {
		return CustomClaim{}, fmt.Errorf("secret key not found")
	}

	claims := CustomClaim{}

	_, err := jwt.ParseWithClaims(ts, &claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("invalid sign method")
		}

		return []byte(secret), nil
	})

	if err != nil {
		return CustomClaim{}, fmt.Errorf("failed to parse token: %v", err)
	}

	return claims, nil
}
