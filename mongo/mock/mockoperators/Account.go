// Code generated by mockery v2.32.0. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
	db "gitlab.com/leducthai/simple_mongodb/services/db"
)

// Account is an autogenerated mock type for the Account type
type Account struct {
	mock.Mock
}

// CreateAccount provides a mock function with given fields: _a0, _a1
func (_m *Account) CreateAccount(_a0 context.Context, _a1 db.CreateAccountRequset) error {
	ret := _m.Called(_a0, _a1)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, db.CreateAccountRequset) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// IsInBlackList provides a mock function with given fields: _a0, _a1
func (_m *Account) IsInBlackList(_a0 context.Context, _a1 string) (bool, error) {
	ret := _m.Called(_a0, _a1)

	var r0 bool
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) (bool, error)); ok {
		return rf(_a0, _a1)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) bool); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Get(0).(bool)
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Login provides a mock function with given fields: _a0, _a1
func (_m *Account) Login(_a0 context.Context, _a1 db.LoginReq) (db.LoginReply, error) {
	ret := _m.Called(_a0, _a1)

	var r0 db.LoginReply
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, db.LoginReq) (db.LoginReply, error)); ok {
		return rf(_a0, _a1)
	}
	if rf, ok := ret.Get(0).(func(context.Context, db.LoginReq) db.LoginReply); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Get(0).(db.LoginReply)
	}

	if rf, ok := ret.Get(1).(func(context.Context, db.LoginReq) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Logout provides a mock function with given fields: _a0, _a1
func (_m *Account) Logout(_a0 context.Context, _a1 db.LogoutRequest) error {
	ret := _m.Called(_a0, _a1)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, db.LogoutRequest) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ResetToken provides a mock function with given fields: _a0, _a1
func (_m *Account) ResetToken(_a0 context.Context, _a1 db.LogoutRequest) (db.LoginReply, error) {
	ret := _m.Called(_a0, _a1)

	var r0 db.LoginReply
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, db.LogoutRequest) (db.LoginReply, error)); ok {
		return rf(_a0, _a1)
	}
	if rf, ok := ret.Get(0).(func(context.Context, db.LogoutRequest) db.LoginReply); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Get(0).(db.LoginReply)
	}

	if rf, ok := ret.Get(1).(func(context.Context, db.LogoutRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// NewAccount creates a new instance of Account. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewAccount(t interface {
	mock.TestingT
	Cleanup(func())
}) *Account {
	mock := &Account{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
