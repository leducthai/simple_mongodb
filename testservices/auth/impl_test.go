package auth

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/leducthai/simple_mongodb/mongo/operations/auth"
	"gitlab.com/leducthai/simple_mongodb/services/db"
	"gitlab.com/leducthai/simple_mongodb/utils/testservices"
)

type CusSuite struct {
	testservices.SuiteConfig
}

func NewCusSuite() *CusSuite {
	s := testservices.NewSuite(testservices.SuiteConfigParams{
		RelatedCollections: []string{
			"accounts",
			"blacklist",
		},
	})
	return &CusSuite{
		SuiteConfig: *s,
	}
}

func TestSuite(t *testing.T) {
	suite.Run(t, NewCusSuite())
}

func (c *CusSuite) TestCreateAccount() {
	assert := c.Assertions
	sv := c.GetSV()
	ctx := context.Background()

	// Arrange
	newAccount := db.CreateAccountRequset{
		Email:      "thai@it.com",
		Password:   "password",
		Permisions: []int32{1, 2},
	}

	// Act
	err := sv.AccountService().CreateAccount(ctx, newAccount)

	// Assert
	assert.NoError(err)
}

func (c *CusSuite) TestLogin() {
	assert := c.Assertions
	sv := c.GetSV()
	ctx := context.Background()
	{ // good case
		// Arrange
		newAccount := db.CreateAccountRequset{
			Email:      "thai@it.com",
			Password:   "password",
			Permisions: []int32{1, 2},
		}
		sv.AccountService().CreateAccount(ctx, newAccount)

		// Act
		_, err := sv.AccountService().Login(ctx, db.LoginReq{
			Email:    "thai@it.com",
			Password: "password",
		})

		// Assert
		assert.NoError(err)
	}
	//bad cases
	{ // wrong password
		// Arrange

		// Act
		_, err := sv.AccountService().Login(ctx, db.LoginReq{
			Email:    "thai@it.com",
			Password: "wrong_password",
		})

		// Assert
		assert.Error(err)
		assert.Equal(err, fmt.Errorf("wrong password or email"))
	}
	{ // no account founded
		// Arange
		c.ClearData()

		// Act
		_, err := sv.AccountService().Login(ctx, db.LoginReq{
			Email:    "thai@it.com",
			Password: "password",
		})

		// Assert
		assert.Error(err)
	}
}

func (c *CusSuite) TestLogout() {
	assert := c.Assertions
	sv := c.GetSV()
	ctx := context.Background()

	{ // good cases
		// Arrange
		newAccount := db.CreateAccountRequset{
			Email:      "thai@it.com",
			Password:   "password",
			Permisions: []int32{1, 2},
		}
		sv.AccountService().CreateAccount(ctx, newAccount)
		token, _ := sv.AccountService().Login(ctx, db.LoginReq{
			Email:    "thai@it.com",
			Password: "password",
		})

		// Act
		err := sv.AccountService().Logout(ctx, db.LogoutRequest{
			Token: token.Token,
		})

		// Assert
		assert.NoError(err)
	}
}

func (c *CusSuite) TestResetToken() {
	assert := c.Assertions
	ctx := context.Background()
	sv := c.GetSV()

	// good case
	{
		// Arrange
		newAccount := db.CreateAccountRequset{
			Email:      "thai@it.com",
			Password:   "password",
			Permisions: []int32{1, 2},
		}
		sv.AccountService().CreateAccount(ctx, newAccount)
		token, _ := sv.AccountService().Login(ctx, db.LoginReq{
			Email:    "thai@it.com",
			Password: "password",
		})
		_, err := auth.VerifyToken(token.Token)
		assert.NoError(err)

		// Act
		newToken, err := sv.AccountService().ResetToken(ctx, db.LogoutRequest{Token: token.Token})

		// Assert
		assert.NoError(err)
		_, err = auth.VerifyToken(newToken.Token)
		assert.NoError(err)
		isInBL, err := sv.AccountService().IsInBlackList(ctx, token.Token)
		assert.NoError(err)
		assert.Equal(isInBL, true)
	}
}
