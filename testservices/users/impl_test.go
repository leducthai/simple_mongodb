package users

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/leducthai/simple_mongodb/services/db"
	"gitlab.com/leducthai/simple_mongodb/utils/testservices"
)

type CusSuite struct {
	testservices.SuiteConfig
}

func NewCusSuite() *CusSuite {
	s := testservices.NewSuite(testservices.SuiteConfigParams{
		RelatedCollections: []string{
			"users",
		},
	})
	return &CusSuite{
		SuiteConfig: *s,
	}
}

func TestSuite(t *testing.T) {
	suite.Run(t, NewCusSuite())
}

func (c CusSuite) TestGetUserByOwner() {
	assert := c.Assertions
	sv := c.GetSV()
	ctx := context.Background()

	// good cases
	{ // owner get infor
		// Arrange
		err := sv.UserService().CreateUser(ctx, db.CreateUserRequset{
			Email: "test@gmail.com",
			Name:  "testName",
			Age:   22,
			Bio:   "this is the test case",
		})
		assert.NoError(err)

		// Act
		reply, err := sv.UserService().GetUser(ctx, "test@gmail.com", true)

		// Assert
		expected := db.GetUserReply{
			Name: "testName",
			Age:  22,
			Bio:  "this is the test case",
		}
		assert.NoError(err)
		assert.Equal(expected, reply)
		c.ClearData()
	}
	{ // admin get user
		// Arrange
		err := sv.UserService().CreateUser(ctx, db.CreateUserRequset{
			Email: "test@gmail.com",
			Name:  "testName",
			Age:   22,
			Bio:   "this is the test case",
		})
		assert.NoError(err)

		// Act
		reply, err := sv.UserService().GetUser(ctx, "test@gmail.com", false)

		// Assert
		expected := db.GetUserReplyAdmin{
			Name: "testName",
			Bio:  "this is the test case",
		}
		assert.NoError(err)
		assert.Equal(expected, reply)
		c.ClearData()
	}

	// bad case
	{ // not found
		// Arrange

		// Act
		_, err := sv.UserService().GetUser(ctx, "test@gmail.com", true)

		// Assert
		assert.Error(err)
		assert.Equal(fmt.Errorf("no matching document found"), err)
	}
}

func (c CusSuite) TestCreateUser() {
	assert := c.Assertions
	ctx := context.Background()
	sv := c.GetSV()

	//good cases
	{
		// Arrange

		// Act
		err := sv.UserService().CreateUser(ctx, db.CreateUserRequset{
			Email: "test@yahooh.com",
			Name:  "testName",
			Age:   22,
			Bio:   "this is the test case",
		})

		// Assert
		assert.NoError(err)
		reply, err := sv.UserService().GetUser(ctx, "test@yahooh.com", true)
		assert.NoError(err)
		expected := db.GetUserReply{
			Name: "testName",
			Age:  22,
			Bio:  "this is the test case",
		}
		assert.Equal(expected, reply)
		c.ClearData()
	}
	{ // missing fields
		// Arrange

		// Act
		err := sv.UserService().CreateUser(ctx, db.CreateUserRequset{
			Email: "test@yahooh.com",
			Name:  "testName",
		})

		// Assert
		assert.NoError(err)
		reply, err := sv.UserService().GetUser(ctx, "test@yahooh.com", true)
		assert.NoError(err)
		expected := db.GetUserReply{
			Name: "testName",
		}
		assert.Equal(expected, reply)
		c.ClearData()
	}

	// bad cases
	{ // duplicate email
		// Arrange
		err := sv.UserService().CreateUser(ctx, db.CreateUserRequset{
			Email: "test@yahooh.com",
		})
		assert.NoError(err)

		// Act
		err = sv.UserService().CreateUser(ctx, db.CreateUserRequset{
			Email: "test@yahooh.com",
			Name:  "testName",
		})

		// Assert
		assert.Error(err)
	}
}

func (c CusSuite) TestDeleteUser() {
	assert := c.Assertions
	ctx := context.Background()
	sv := c.GetSV()

	// good case
	{
		// Arrange
		err := sv.UserService().CreateUser(ctx, db.CreateUserRequset{
			Email: "test@yahooh.com",
			Name:  "testName",
		})
		assert.NoError(err)

		// Act
		rp, err := sv.UserService().DeleteUser(ctx, "test@yahooh.com")

		// Assert
		assert.NoError(err)
		assert.Equal(int64(1), rp.RowAffected)
	}

	// bad case
	{
		// Arrange

		// Act
		rp, err := sv.UserService().DeleteUser(ctx, "test@yahooh.com")

		// Assert
		assert.Error(err)
		assert.Equal(fmt.Errorf("not found"), err)
		assert.Equal(int64(0), rp.RowAffected)
	}
}

func (c *CusSuite) TestUpdateUser() {
	assert := c.Assertions
	sv := c.GetSV()
	ctx := context.Background()

	// good case
	{
		// Arrange
		err := sv.UserService().CreateUser(ctx, db.CreateUserRequset{
			Email: "test@gmail.com",
			Name:  "test",
			Age:   22,
			Bio:   "this is the test case",
		})
		assert.NoError(err)

		// Act
		reply, err := sv.UserService().UpdateUser(ctx, "test@gmail.com", db.UpsertUsertRequest{
			Name: "thai",
			Age:  22,
			Bio:  "new bio",
		})

		// Assert
		assert.NoError(err)
		assert.Equal(int64(1), reply.RowAffected)
		expect := db.GetUserReply{
			Name: "thai",
			Age:  22,
			Bio:  "new bio",
		}
		actual, err := sv.UserService().GetUser(ctx, "test@gmail.com", true)
		assert.NoError(err)
		assert.Equal(expect, actual)
		c.ClearData()
	}

	// bad cases
	{ // not found
		// Arrange

		// Act
		reply, err := sv.UserService().UpdateUser(ctx, "test@gmail.com", db.UpsertUsertRequest{
			Name: "thai",
			Age:  22,
			Bio:  "new bio",
		})

		// Assert
		assert.Error(err)
		assert.Equal(fmt.Errorf("not found"), err)
		assert.Equal(int64(0), reply.RowAffected)
	}
}
