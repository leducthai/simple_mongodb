package handlertest

import (
	"bytes"
	"net/http"
	"net/http/httptest"

	"github.com/labstack/echo"
)

func NewContext(url string, request []byte, opt ...option) (echo.Context, *httptest.ResponseRecorder) {
	// Create a new Echo instance
	e := echo.New()

	// create request body
	body := bytes.NewReader(request)

	// Create a new HTTP request
	req := httptest.NewRequest(http.MethodGet, url, body)

	// Set the "Content-Type" header to "application/json"
	req.Header.Set("Content-Type", "application/json")

	// Set required Header
	parseOptions(req, opt)

	// Create a new HTTP response recorder
	rec := httptest.NewRecorder()

	// Create a new Echo context
	c := e.NewContext(req, rec)

	// return both echo context and httptest recorder for convenience
	return c, rec
}

type option func(*http.Request)

func parseOptions(req *http.Request, opt []option) *http.Request {
	for _, o := range opt {
		o(req)
	}
	return req
}

func SetAuth(req *http.Request) {
	req.Header.Set("Authorization", "randomauthkeyfortesing")
}
