package testservices

import (
	"context"
	"log"

	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/suite"
	"gitlab.com/leducthai/simple_mongodb/services/api"
	"gitlab.com/leducthai/simple_mongodb/services/db"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type SuiteConfigParams struct {
	RelatedCollections []string
}

type SuiteConfig struct {
	*suite.Suite
	RelatedCollections []string
	dm                 db.DataManager
	db                 *mongo.Database
	rd                 *redis.Client
	sv                 api.ServicesInter
}

func NewSuite(ps SuiteConfigParams) *SuiteConfig {
	return &SuiteConfig{
		RelatedCollections: ps.RelatedCollections,
		Suite:              &suite.Suite{},
	}
}

func (s *SuiteConfig) SetupSuite() {
	dm, sv, db, red := InitializeDB()
	s.dm = *dm
	s.db = db
	s.sv = sv
	s.rd = red
}

func (s *SuiteConfig) SetupTest() {
	s.ClearData()
}

func (s *SuiteConfig) TearDownSuite() {
	s.dm.Close()
}

func (s *SuiteConfig) ClearData() {
	for _, col := range s.RelatedCollections {
		_, err := s.db.Collection(col).DeleteMany(context.Background(), bson.D{{}})
		if err != nil {
			log.Fatal(err)
		}
	}
	s.rd.FlushDB(context.Background())
}

func (s *SuiteConfig) GetSV() api.ServicesInter {
	return s.sv
}
