package testservices

import (
	"fmt"

	"github.com/jessevdk/go-flags"
	"github.com/redis/go-redis/v9"
	mg "gitlab.com/leducthai/simple_mongodb/mongo"
	"gitlab.com/leducthai/simple_mongodb/services/api"
	"gitlab.com/leducthai/simple_mongodb/services/db"
	"go.mongodb.org/mongo-driver/mongo"
)

var DBOptions struct {
	Host       string `long:"host" description:"the host in which the mongo server runs" default:"localhost" env:"MONGO_HOST"`
	Port       int    `long:"port" description:"the port in which the mongo server runs" default:"27017" env:"MONGO_PORT"`
	DBUserName string `long:"db-username" description:"the username of mongodb server" default:"thai_pro" env:"MONGO_USERNAME"`
	DBPassword string `long:"db-password" description:"the password of mongodb server" default:"thai_pass" env:"MONGO_PASSWORD"`

	RedisConnect  string `long:"redis-connect" description:"the address of redis server" default:"198.1.1.222:6379" env:"REDIS_CONNECT"`
	RedisPassword string `long:"redis-password" description:"the password of redis server" default:"edit_your_own_password_for_redis" env:"REDIS_PASSWORD"`
}

func checkOptions() error {
	parser := flags.NewParser(&DBOptions, flags.IgnoreUnknown)
	if _, err := parser.Parse(); err != nil {
		if fe, ok := err.(*flags.Error); ok && fe.Type == flags.ErrHelp {
			return nil
		}
		return fmt.Errorf("fail to parse options")
	}

	return nil
}

func InitializeDB() (*db.DataManager, api.ServicesInter, *mongo.Database, *redis.Client) {
	checkOptions()

	config := mg.MongoConfig{
		Address:  DBOptions.Host,
		Port:     DBOptions.Port,
		Username: DBOptions.DBUserName,
		Password: DBOptions.DBPassword,

		RedisConnect: DBOptions.RedisConnect,
		RedisPass:    DBOptions.RedisPassword,
	}

	dm := mg.New(config, "test_db", 1)

	services := api.Services{
		User:    dm.UserService(),
		Account: dm.Account(),
	}
	_, db := mg.NewDB(config, "test_db")

	red := mg.NewRedis(config, 1)

	return &dm, services, db, red
}
