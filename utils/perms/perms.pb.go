// Code generated by protoc-gen-go. DO NOT EDIT.
// source: perms.proto

package perms

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type FuncNames int32

const (
	FuncNames_GetUser       FuncNames = 0
	FuncNames_GetUserID     FuncNames = 1
	FuncNames_CreateUser    FuncNames = 2
	FuncNames_DeleteUser    FuncNames = 3
	FuncNames_UpdateUser    FuncNames = 4
	FuncNames_Login         FuncNames = 5
	FuncNames_Logout        FuncNames = 6
	FuncNames_ReFresh       FuncNames = 7
	FuncNames_CreateAccount FuncNames = 8
)

var FuncNames_name = map[int32]string{
	0: "GetUser",
	1: "GetUserID",
	2: "CreateUser",
	3: "DeleteUser",
	4: "UpdateUser",
	5: "Login",
	6: "Logout",
	7: "ReFresh",
	8: "CreateAccount",
}

var FuncNames_value = map[string]int32{
	"GetUser":       0,
	"GetUserID":     1,
	"CreateUser":    2,
	"DeleteUser":    3,
	"UpdateUser":    4,
	"Login":         5,
	"Logout":        6,
	"ReFresh":       7,
	"CreateAccount": 8,
}

func (x FuncNames) String() string {
	return proto.EnumName(FuncNames_name, int32(x))
}

func (FuncNames) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_05a77249dc64651d, []int{0}
}

type Roles int32

const (
	Roles_User  Roles = 0
	Roles_Admin Roles = 1
	Roles_Dev   Roles = 2
)

var Roles_name = map[int32]string{
	0: "User",
	1: "Admin",
	2: "Dev",
}

var Roles_value = map[string]int32{
	"User":  0,
	"Admin": 1,
	"Dev":   2,
}

func (x Roles) String() string {
	return proto.EnumName(Roles_name, int32(x))
}

func (Roles) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_05a77249dc64651d, []int{1}
}

func init() {
	proto.RegisterEnum("perms.FuncNames", FuncNames_name, FuncNames_value)
	proto.RegisterEnum("perms.Roles", Roles_name, Roles_value)
}

func init() { proto.RegisterFile("perms.proto", fileDescriptor_05a77249dc64651d) }

var fileDescriptor_05a77249dc64651d = []byte{
	// 188 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x34, 0xcf, 0x4b, 0x6a, 0x84, 0x40,
	0x10, 0x80, 0xe1, 0xf8, 0x68, 0xb5, 0x4b, 0x0c, 0x95, 0xba, 0x43, 0x36, 0x2e, 0xb2, 0xc9, 0x09,
	0x24, 0x62, 0x08, 0x48, 0x16, 0x82, 0x07, 0x70, 0xb4, 0x70, 0x04, 0xed, 0x96, 0xee, 0x76, 0x8e,
	0x31, 0x67, 0x1e, 0x74, 0x9c, 0xe5, 0xc7, 0x0f, 0xf5, 0x80, 0x74, 0x65, 0xb3, 0xd8, 0xaf, 0xd5,
	0x68, 0xa7, 0x49, 0x1c, 0xc8, 0xef, 0x1e, 0xc8, 0x6a, 0x53, 0xfd, 0x7f, 0xb7, 0xb0, 0xa5, 0x14,
	0xe2, 0x5f, 0x76, 0xad, 0x65, 0x83, 0x6f, 0x94, 0x81, 0x3c, 0xf1, 0x57, 0xa2, 0x47, 0xef, 0x00,
	0x3f, 0x86, 0x3b, 0xc7, 0x47, 0xf6, 0x77, 0x97, 0x3c, 0xf3, 0xe9, 0x60, 0x77, 0xbb, 0x0e, 0xaf,
	0x1e, 0x92, 0x04, 0x51, 0xeb, 0x71, 0x52, 0x28, 0x08, 0x20, 0xaa, 0xf5, 0xa8, 0x37, 0x87, 0xd1,
	0xbe, 0xa2, 0xe1, 0xca, 0xb0, 0xbd, 0x62, 0x4c, 0x1f, 0x90, 0x3d, 0x67, 0x16, 0x7d, 0xaf, 0x37,
	0xe5, 0x30, 0xc9, 0x3f, 0x41, 0x34, 0x7a, 0x66, 0x4b, 0x09, 0x84, 0xe7, 0x21, 0x12, 0x44, 0x31,
	0x2c, 0x93, 0x42, 0x8f, 0x62, 0x08, 0x4a, 0xbe, 0xa1, 0x7f, 0x89, 0x8e, 0x2f, 0xbe, 0x1f, 0x01,
	0x00, 0x00, 0xff, 0xff, 0x14, 0x47, 0x61, 0x7f, 0xd4, 0x00, 0x00, 0x00,
}
