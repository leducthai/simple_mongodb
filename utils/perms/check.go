package perms

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v3"
)

type permissions map[string][]string

var Config struct {
	Init map[string]string `yaml:"inituser"`
	Perm permissions       `yaml:"permissions"`
}

func LoadYaml(yamlPath string) error {
	file, err := ioutil.ReadFile(yamlPath)
	if err != nil {
		return err
	}

	if err := yaml.Unmarshal(file, &Config); err != nil {
		return err
	}
	return nil
}

func HasPermission(fname string, rol []int32) (bool, error) {
	rolesAllowed, ok := Config.Perm[fname]
	if !ok {
		return false, fmt.Errorf("wrong function name")
	}
	for _, r := range rolesAllowed {
		for _, ro := range rol {
			if int32(Roles_value[r]) == ro {
				return true, nil
			}
		}
	}
	return false, nil
}
