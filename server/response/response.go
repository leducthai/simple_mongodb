package response

type CusRes struct {
	Detail string `json:"detail"`
}

func NewCusRes(res string) CusRes {
	return CusRes{
		Detail: res,
	}
}

type ResponseOK struct {
}
