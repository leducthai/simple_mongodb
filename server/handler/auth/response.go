package auth

import (
	"context"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/leducthai/simple_mongodb/server/response"
	"gitlab.com/leducthai/simple_mongodb/services/api"
	"gitlab.com/leducthai/simple_mongodb/services/db"
	"gitlab.com/leducthai/simple_mongodb/utils/perms"
)

func LoginHandle(s api.ServicesInter) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		instance := new(db.LoginReq)
		if err := ctx.Bind(instance); err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(err.Error()))
		}
		rt, err := s.AccountService().Login(context.Background(), *instance)
		if err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(err.Error()))
		}
		return ctx.JSON(http.StatusOK, rt)
	}
}

func LogoutHandle(s api.ServicesInter) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		p := ctx.Get("permissions").([]int32)

		hasPerms, p_err := perms.HasPermission(perms.FuncNames_name[int32(perms.FuncNames_Logout)], p)
		if p_err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(p_err.Error()))
		}
		if !hasPerms {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes("unauthorize"))
		}

		tokenStr := ctx.Request().Header.Get("Authorization")
		err := s.AccountService().Logout(context.Background(), db.LogoutRequest{Token: tokenStr})
		if err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(err.Error()))
		}
		return ctx.JSON(http.StatusOK, response.NewCusRes("Logged out"))
	}
}

func RefreshTokenHandle(s api.ServicesInter) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		p := ctx.Get("permissions").([]int32)

		hasPerms, p_err := perms.HasPermission(perms.FuncNames_name[int32(perms.FuncNames_ReFresh)], p)
		if p_err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(p_err.Error()))
		}
		if !hasPerms {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes("unauthorize"))
		}

		tokenStr := ctx.Request().Header.Get("Authorization")
		rt, err := s.AccountService().ResetToken(context.Background(), db.LogoutRequest{Token: tokenStr})
		if err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(err.Error()))
		}
		return ctx.JSON(http.StatusOK, rt)
	}
}

func CreateAccountHandle(s api.ServicesInter) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		p := ctx.Get("permissions").([]int32)

		hasPerms, p_err := perms.HasPermission(perms.FuncNames_name[int32(perms.FuncNames_CreateAccount)], p)
		if p_err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(p_err.Error()))
		}
		if !hasPerms {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes("unauthorize"))
		}

		instance := new(db.CreateAccountRequset)
		if err := ctx.Bind(instance); err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(err.Error()))
		}
		err := s.AccountService().CreateAccount(context.Background(), *instance)
		if err != nil {
			return ctx.JSON(http.StatusBadGateway, response.NewCusRes(err.Error()))
		}
		return ctx.JSON(http.StatusOK, response.NewCusRes("ok"))
	}
}
