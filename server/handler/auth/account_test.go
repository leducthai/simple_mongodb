package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	mocks "gitlab.com/leducthai/simple_mongodb/mongo/mock/mockoperators"
	"gitlab.com/leducthai/simple_mongodb/server/response"
	"gitlab.com/leducthai/simple_mongodb/services/db"
	handlertest "gitlab.com/leducthai/simple_mongodb/utils/handler_test"
	"gitlab.com/leducthai/simple_mongodb/utils/perms"
)

type CusSuite struct {
	*suite.Suite
}

func (c CusSuite) SetupSuite() {
	perms.LoadYaml("../../../utils/perms/perm.yaml")
}

func NewCusSuite() *CusSuite {
	return &CusSuite{
		Suite: &suite.Suite{},
	}
}

func TestSuite(t *testing.T) {
	suite.Run(t, NewCusSuite())
}

func (c *CusSuite) Test_Login() {
	assertion := c.Assertions

	{ // goood case
		loginRequest := db.LoginReq{
			Email:    "test@gmail.com",
			Password: "password",
		}
		body, err := json.Marshal(loginRequest)
		assertion.NoError(err)
		ctx, rec := handlertest.NewContext("/testing", body)
		accountFunc := func() db.Account {
			a := mocks.NewAccount(c.T())
			a.On("Login", context.Background(), loginRequest).Return(db.LoginReply{Token: "thisisthetoken"}, nil)
			return a
		}
		services := mocks.NewServicesInter(c.T())
		services.On("AccountService").Return(accountFunc)

		// Act
		err = LoginHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(db.LoginReply{Token: "thisisthetoken"})
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
	// bad cases
	{ // login failed
		// Arrange
		ctx, rec := handlertest.NewContext("/testing", nil)
		errReply := fmt.Errorf("failed to login")
		accountFunc := func() db.Account {
			a := mocks.NewAccount(c.T())
			a.On("Login", context.Background(), db.LoginReq{}).Return(db.LoginReply{}, errReply)
			return a
		}
		services := mocks.NewServicesInter(c.T())
		services.On("AccountService").Return(accountFunc)

		// Act
		err := LoginHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("failed to login"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
}

func (c *CusSuite) Test_Logout() {
	assertion := c.Assertions

	// good case
	{
		// Arrange
		ctx, rec := handlertest.NewContext("/testing", nil, handlertest.SetAuth)
		ctx.Set("permissions", []int32{0})
		logoutReq := db.LogoutRequest{Token: "randomauthkeyfortesing"}
		accountFunc := func() db.Account {
			a := mocks.NewAccount(c.T())
			a.On("Logout", context.Background(), logoutReq).Return(nil)
			return a
		}
		services := mocks.NewServicesInter(c.T())
		services.On("AccountService").Return(accountFunc)

		// Act
		err := LogoutHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("Logged out"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}

	// bad cases
	{ // no permission
		// Arrange
		ctx, rec := handlertest.NewContext("/tesing", nil)
		ctx.Set("permissions", []int32{})
		services := mocks.NewServicesInter(c.T())

		// Act
		err := LogoutHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("unauthorize"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
	{ // failed to logout
		//Arrange
		ctx, rec := handlertest.NewContext("/testing", nil, handlertest.SetAuth)
		ctx.Set("permissions", []int32{0})
		logoutReq := db.LogoutRequest{Token: "randomauthkeyfortesing"}
		errReply := fmt.Errorf("failed to logout")
		accountFunc := func() db.Account {
			a := mocks.NewAccount(c.T())
			a.On("Logout", context.Background(), logoutReq).Return(errReply)
			return a
		}
		services := mocks.NewServicesInter(c.T())
		services.On("AccountService").Return(accountFunc)

		// Act
		err := LogoutHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("failed to logout"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
}

func (c *CusSuite) Test_RefreshToken() {
	assertion := c.Assertions

	// good case
	{
		// Arrange
		ctx, rec := handlertest.NewContext("/testing", nil, handlertest.SetAuth)
		ctx.Set("permissions", []int32{0, 1, 2})
		refreshReq := db.LogoutRequest{Token: "randomauthkeyfortesing"}
		refreshReply := db.LoginReply{Token: "newtokenaffterrefresh"}
		accountFunc := func() db.Account {
			a := mocks.NewAccount(c.T())
			a.On("ResetToken", context.Background(), refreshReq).Return(refreshReply, nil)
			return a
		}
		services := mocks.NewServicesInter(c.T())
		services.On("AccountService").Return(accountFunc)

		// Act
		err := RefreshTokenHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(refreshReply)
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}

	// bad cases
	{ // no permission
		// Arrange
		ctx, rec := handlertest.NewContext("/tesing", nil)
		ctx.Set("permissions", []int32{})
		services := mocks.NewServicesInter(c.T())

		// Act
		err := RefreshTokenHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("unauthorize"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
	{ // failed to refresh token
		//Arrange
		ctx, rec := handlertest.NewContext("/testing", nil, handlertest.SetAuth)
		ctx.Set("permissions", []int32{0, 1, 2})
		refreshReq := db.LogoutRequest{Token: "randomauthkeyfortesing"}
		errReply := fmt.Errorf("failed to refresh token")
		accountFunc := func() db.Account {
			a := mocks.NewAccount(c.T())
			a.On("ResetToken", context.Background(), refreshReq).Return(db.LoginReply{}, errReply)
			return a
		}
		services := mocks.NewServicesInter(c.T())
		services.On("AccountService").Return(accountFunc)

		// Act
		err := RefreshTokenHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("failed to refresh token"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}

}

func (c *CusSuite) TestCreateAccount() {
	assertion := c.Assertions

	// good case
	{
		// Arrange
		newAccount := db.CreateAccountRequset{
			Email:      "test@gmail.com",
			Password:   "password",
			Permisions: []int32{0, 1, 2},
		}
		body, err := json.Marshal(newAccount)
		assertion.NoError(err)
		ctx, rec := handlertest.NewContext("/tesing", body)
		ctx.Set("permissions", []int32{0, 1, 2})
		accountFunc := func() db.Account {
			a := mocks.NewAccount(c.T())
			a.On("CreateAccount", context.Background(), newAccount).Return(nil)
			return a
		}
		services := mocks.NewServicesInter(c.T())
		services.On("AccountService").Return(accountFunc)

		// Act
		err = CreateAccountHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("ok"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}

	// bad cases
	{ // no permission
		// Arrange
		ctx, rec := handlertest.NewContext("/tesing", nil)
		ctx.Set("permissions", []int32{})
		services := mocks.NewServicesInter(c.T())

		// Act
		err := CreateAccountHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("unauthorize"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
	{ // failed to refresh token
		//Arrange
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("permissions", []int32{0, 1, 2})
		errReply := fmt.Errorf("failed to refresh token")
		accountFunc := func() db.Account {
			a := mocks.NewAccount(c.T())
			a.On("CreateAccount", context.Background(), db.CreateAccountRequset{}).Return(errReply)
			return a
		}
		services := mocks.NewServicesInter(c.T())
		services.On("AccountService").Return(accountFunc)

		// Act
		err := CreateAccountHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("failed to refresh token"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
}
