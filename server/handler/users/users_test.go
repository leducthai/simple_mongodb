package user

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	mocks "gitlab.com/leducthai/simple_mongodb/mongo/mock/mockoperators"
	"gitlab.com/leducthai/simple_mongodb/server/response"
	"gitlab.com/leducthai/simple_mongodb/services/db"
	handlertest "gitlab.com/leducthai/simple_mongodb/utils/handler_test"
	"gitlab.com/leducthai/simple_mongodb/utils/perms"
)

type CusSuite struct {
	*suite.Suite
}

func (c CusSuite) SetupSuite() {
	perms.LoadYaml("../../../utils/perms/perm.yaml")
}

func NewCusSuite() *CusSuite {
	return &CusSuite{
		Suite: &suite.Suite{},
	}
}

func TestSuite(t *testing.T) {
	suite.Run(t, NewCusSuite())
}

func (c *CusSuite) Test_GetUserHandler() {
	assertion := c.Assertions
	// good cases
	{ // owner call
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("email", "fake@gmail.com")
		ctx.Set("permissions", []int32{0, 1, 2})

		replyUser := db.GetUserReply{
			Name: "fakeName",
			Age:  22,
			Bio:  "testing mock",
		}
		userFunc := func() db.User {
			u := mocks.NewUser(c.T())
			u.On("GetUser", context.Background(), "fake@gmail.com", true).Return(replyUser, nil)
			return u
		}

		services := mocks.NewServicesInter(c.T())
		services.On("UserService").Return(userFunc)
		err := GetUserHandle(services)(ctx)
		assertion.NoError(err)

		expect, err := json.Marshal(replyUser)
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
	{ // admin call
		ctx, rec := handlertest.NewContext("/testing/?email=user@gmail.com", nil)
		ctx.Set("email", "admin@gmail.com")
		ctx.Set("permissions", []int32{0, 1, 2})

		replyUser := db.GetUserReplyAdmin{
			Name: "fakeName",
			Bio:  "testing mock",
		}
		userFunc := func() db.User {
			u := mocks.NewUser(c.T())
			u.On("GetUser", context.Background(), "user@gmail.com", false).Return(replyUser, nil)
			return u
		}

		services := mocks.NewServicesInter(c.T())
		services.On("UserService").Return(userFunc)
		err := GetUserHandle(services)(ctx)
		assertion.NoError(err)

		expect, err := json.Marshal(replyUser)
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}

	//bad cases
	{ // don't have permission
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("email", "fake@gmail.com")
		ctx.Set("permissions", []int32{})

		services := mocks.NewServicesInter(c.T())
		err := GetUserHandle(services)(ctx)
		assertion.NoError(err)

		expected, err := json.Marshal(response.NewCusRes("unauthorize"))
		assertion.NoError(err)
		assertion.Equal(string(expected)+"\n", rec.Body.String())
	}
	{ // failed to get user
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("email", "user@gmail.com")
		ctx.Set("permissions", []int32{0, 1, 2})

		replyUser := db.GetUserReply{}
		replyErr := fmt.Errorf("failed to get user")
		userFunc := func() db.User {
			u := mocks.NewUser(c.T())
			u.On("GetUser", context.Background(), "user@gmail.com", true).Return(replyUser, replyErr)
			return u
		}

		services := mocks.NewServicesInter(c.T())
		services.On("UserService").Return(userFunc)
		err := GetUserHandle(services)(ctx)
		assertion.NoError(err)

		expected, err := json.Marshal(response.NewCusRes("failed to get user"))
		assertion.NoError(err)
		assertion.Equal(string(expected)+"\n", rec.Body.String())
	}
}

func (c *CusSuite) Test_CreateUser() {
	assertion := c.Assertions

	// good cases
	{ // good cases
		// Arrange
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("email", "user@gmail.com")
		ctx.Set("permissions", []int32{0, 1, 2})

		newUserParams := db.CreateUserRequset{
			Email: "user@gmail.com",
		}
		userFunc := func() db.User {
			u := mocks.NewUser(c.T())
			u.On("CreateUser", context.Background(), newUserParams).Return(nil)
			return u
		}
		services := mocks.NewServicesInter(c.T())
		services.On("UserService").Return(userFunc)

		// Act
		err := CreateUserHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("insert ok"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}

	// bad cases
	{ // don't have permission
		// Arrange
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("email", "user@gmail.com")
		ctx.Set("permissions", []int32{})

		services := mocks.NewServicesInter(c.T())

		// Act
		err := CreateUserHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("unauthorize"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
	{ // failed to create user
		// Arrange
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("email", "user@gmail.com")
		ctx.Set("permissions", []int32{0, 1, 2})
		errReply := fmt.Errorf("failed to create user")
		userFunc := func() db.User {
			u := mocks.NewUser(c.T())
			u.On("CreateUser", context.Background(), db.CreateUserRequset{Email: "user@gmail.com"}).Return(errReply)
			return u
		}
		services := mocks.NewServicesInter(c.T())
		services.On("UserService").Return(userFunc)

		// Act
		err := CreateUserHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("failed to create user"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
}

func (c *CusSuite) Test_DeleteUser() {
	assertion := c.Assertions

	// good case
	{
		// Arrange
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("email", "user@gmail.com")
		ctx.Set("permissions", []int32{0, 1, 2})
		userFunc := func() db.User {
			u := mocks.NewUser(c.T())
			u.On("DeleteUser", context.Background(), "user@gmail.com").Return(db.CommonUpsertReply{}, nil)
			return u
		}
		services := mocks.NewServicesInter(c.T())
		services.On("UserService").Return(userFunc)

		// Act
		err := DeleteUserHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("delete ok"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}

	// bad cases
	{ // no permission
		// Arrange
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("permissions", []int32{})
		services := mocks.NewServicesInter(c.T())

		// Act
		err := DeleteUserHandle(services)(ctx)

		// Arrange
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("unauthorize"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
	{ // deletion failed
		// Arrange
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("email", "user@gmail.com")
		ctx.Set("permissions", []int32{0, 1, 2})
		errReply := fmt.Errorf("deletion failed")
		userFunc := func() db.User {
			u := mocks.NewUser(c.T())
			u.On("DeleteUser", context.Background(), "user@gmail.com").Return(db.CommonUpsertReply{}, errReply)
			return u
		}
		services := mocks.NewServicesInter(c.T())
		services.On("UserService").Return(userFunc)

		// Act
		err := DeleteUserHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("deletion failed"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
}

func (c *CusSuite) Test_UpdateUser() {
	assertion := c.Assertions

	{ // good case
		// Arrange
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("email", "user@gmail.com")
		ctx.Set("permissions", []int32{0, 1, 2})
		userFunc := func() db.User {
			u := mocks.NewUser(c.T())
			u.On("UpdateUser", context.Background(), "user@gmail.com", db.UpsertUsertRequest{}).Return(db.CommonUpsertReply{}, nil)
			return u
		}
		services := mocks.NewServicesInter(c.T())
		services.On("UserService").Return(userFunc)

		// Act
		err := UpdateUserHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("updated document done"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}

	// bad cases
	{ // no permission
		// Arrange
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("email", "user@gmail.com")
		ctx.Set("permissions", []int32{})
		services := mocks.NewServicesInter(c.T())

		// Act
		err := UpdateUserHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("unauthorize"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
	{ // update failed
		// Arrange
		ctx, rec := handlertest.NewContext("/testing", nil)
		ctx.Set("email", "user@gmail.com")
		ctx.Set("permissions", []int32{0, 1, 2})
		errReply := fmt.Errorf("update failed")
		userFunc := func() db.User {
			u := mocks.NewUser(c.T())
			u.On("UpdateUser", context.Background(), "user@gmail.com", db.UpsertUsertRequest{}).Return(db.CommonUpsertReply{}, errReply)
			return u
		}
		services := mocks.NewServicesInter(c.T())
		services.On("UserService").Return(userFunc)

		// Act
		err := UpdateUserHandle(services)(ctx)

		// Assert
		assertion.NoError(err)
		expect, err := json.Marshal(response.NewCusRes("update failed"))
		assertion.NoError(err)
		assertion.Equal(string(expect)+"\n", rec.Body.String())
	}
}
