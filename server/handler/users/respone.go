package user

import (
	"context"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/leducthai/simple_mongodb/server/response"
	"gitlab.com/leducthai/simple_mongodb/services/api"
	"gitlab.com/leducthai/simple_mongodb/services/db"
	"gitlab.com/leducthai/simple_mongodb/utils/perms"
)

func GetUserHandle(s api.ServicesInter) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		p := ctx.Get("permissions").([]int32)
		email := ctx.Get("email").(string)
		params := ctx.QueryParam("email")

		hasPerms, p_err := perms.HasPermission(perms.FuncNames_name[int32(perms.FuncNames_GetUser)], p)
		if p_err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(p_err.Error()))
		}
		if !hasPerms {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes("unauthorize"))
		}

		var (
			err   error
			reply interface{}
		)
		if params != "" && email != params {
			reply, err = s.UserService().GetUser(context.Background(), params, false)
		} else {
			reply, err = s.UserService().GetUser(context.Background(), email, true)
		}
		if err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(err.Error()))
		}
		return ctx.JSON(http.StatusOK, reply)
	}
}

func CreateUserHandle(s api.ServicesInter) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		p := ctx.Get("permissions").([]int32)
		email := ctx.Get("email").(string)
		hasPerms, p_err := perms.HasPermission(perms.FuncNames_name[int32(perms.FuncNames_CreateUser)], p)
		if p_err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(p_err.Error()))
		}
		if !hasPerms {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes("unauthorize"))
		}

		instance := new(db.CreateUserRequset)
		if err := ctx.Bind(instance); err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(err.Error()))
		}
		instance.Email = email
		err := s.UserService().CreateUser(context.Background(), *instance)
		if err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(err.Error()))
		}
		return ctx.JSON(http.StatusOK, response.NewCusRes("insert ok"))
	}
}

func DeleteUserHandle(s api.ServicesInter) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		p := ctx.Get("permissions").([]int32)

		hasPerms, p_err := perms.HasPermission(perms.FuncNames_name[int32(perms.FuncNames_DeleteUser)], p)
		if p_err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(p_err.Error()))
		}
		if !hasPerms {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes("unauthorize"))
		}

		email := ctx.Get("email").(string)
		_, err := s.UserService().DeleteUser(context.Background(), email)
		if err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(err.Error()))
		}
		return ctx.JSON(http.StatusOK, response.NewCusRes("delete ok"))
	}
}

func UpdateUserHandle(s api.ServicesInter) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		p := ctx.Get("permissions").([]int32)

		hasPerms, p_err := perms.HasPermission(perms.FuncNames_name[int32(perms.FuncNames_UpdateUser)], p)
		if p_err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(p_err.Error()))
		}
		if !hasPerms {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes("unauthorize"))
		}

		email := ctx.Get("email").(string)
		instance := new(db.UpsertUsertRequest)
		_, err := s.UserService().UpdateUser(context.Background(), email, *instance)
		if err != nil {
			return ctx.JSON(http.StatusBadRequest, response.NewCusRes(err.Error()))
		}
		return ctx.JSON(http.StatusOK, response.NewCusRes("updated document done"))
	}
}
