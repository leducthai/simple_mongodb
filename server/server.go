package server

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/go-openapi/swag"
	"github.com/jessevdk/go-flags"
	"github.com/labstack/echo"
	"gitlab.com/leducthai/simple_mongodb/mongo"
	"gitlab.com/leducthai/simple_mongodb/server/config"
	"gitlab.com/leducthai/simple_mongodb/server/middleware"
	"gitlab.com/leducthai/simple_mongodb/services/api"
	"gitlab.com/leducthai/simple_mongodb/services/db"
	"gitlab.com/leducthai/simple_mongodb/utils/perms"
)

func parseFlags() *config.Option {
	var conf config.Option

	configuations := []swag.CommandLineOptionsGroup{
		{
			ShortDescription: "Server Configuation",
			LongDescription:  "Server Configuation",
			Options:          &conf,
		},
	}

	parser := flags.NewParser(nil, flags.Default)
	for _, optGroup := range configuations {
		if _, err := parser.AddGroup(optGroup.ShortDescription, optGroup.LongDescription, optGroup.Options); err != nil {
			log.Fatalln(err)
		}
	}

	if _, err := parser.Parse(); err != nil {
		code := 1
		if fe, ok := err.(*flags.Error); ok && fe.Type == flags.ErrHelp {
			code = 0
		}
		os.Exit(code)
	}
	return &conf
}

func initUsers(s api.ServicesInter) {
	config := perms.Config.Init

	// create admin user
	adminUser := strings.Split(config["admin"], "|")
	err := s.AccountService().CreateAccount(context.Background(), db.CreateAccountRequset{
		Email:      adminUser[0],
		Password:   adminUser[1],
		Permisions: []int32{perms.Roles_value["Admin"]},
	})
	if err != nil {
		log.Printf("failed to initialize admin user: %v", err)
	}

	// create dev user
	devUser := strings.Split(config["dev"], "|")
	err = s.AccountService().CreateAccount(context.Background(), db.CreateAccountRequset{
		Email:      devUser[0],
		Password:   devUser[1],
		Permisions: []int32{perms.Roles_value["Dev"]},
	})
	if err != nil {
		log.Printf("failed to initialize dev user: %v", err)
	}
}

func Run() {
	// new echo srever
	e := echo.New()
	// parse flags
	flags := parseFlags()

	// load permissions
	err := perms.LoadYaml("utils/perms/perm.yaml")
	if err != nil {
		log.Fatalf("failed to load config: %v", err)
	}

	// connect mongodb
	mongomanager := mongo.New(mongo.MongoConfig{
		Address:      flags.Host,
		Port:         flags.Port,
		Username:     flags.DBUserName,
		Password:     flags.DBPassword,
		RedisConnect: flags.RedisConnect,
		RedisPass:    flags.RedisPassword,
	}, "thai", 0)

	defer mongomanager.Close()

	connectionString := fmt.Sprintf("%s:%d", flags.EchoHost, flags.EchoPort)
	e.Use(middleware.LogMiddleware)

	services := api.NewServices(mongomanager)

	initUsers(services)

	GroupingUrls(e, services)
	e.Start(connectionString)
}
