package config

type Option struct {
	// Echo configs
	EchoPort int    `long:"eport" description:"the host in which the ehco server runs" default:"2531" env:"ECHO_PORT"`
	EchoHost string `long:"ehost" description:"the host in which the echo server runs" default:"localhost" env:"ECHO_HOST"`

	// Mongo configs
	Host       string `long:"host" description:"the host in which the mongo server runs" default:"localhost" env:"MONGO_HOST"`
	Port       int    `long:"port" description:"the port in which the mongo server runs" default:"27017" env:"MONGO_PORT"`
	DBUserName string `long:"db-username" description:"the username of mongodb server" default:"thai_pro" env:"MONGO_USERNAME"`
	DBPassword string `long:"db-password" description:"the password of mongodb server" default:"thai_pass" env:"MONGO_PASSWORD"`

	// Redis configs
	RedisConnect  string `long:"redis-connect" description:"the address of redis server" default:"198.1.1.222:6379" env:"REDIS_CONNECT"`
	RedisPassword string `long:"redis-password" description:"the password of redis server" default:"edit_your_own_password_for_redis" env:"REDIS_PASSWORD"`
}
