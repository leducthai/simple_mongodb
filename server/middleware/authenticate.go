package middleware

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/leducthai/simple_mongodb/mongo/operations/auth"
	"gitlab.com/leducthai/simple_mongodb/server/response"
	"gitlab.com/leducthai/simple_mongodb/services/api"
)

func Authmidleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// get token from header
		token := c.Request().Header.Get("Authorization")
		if token == "" {
			return c.JSON(http.StatusNonAuthoritativeInfo, response.NewCusRes("NonAuthoritativeInfo"))
		}

		// check if it is in blacklist
		isInBlackList, err := api.GetIsInBlackList(token)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, response.NewCusRes("failed to check token"))
		}
		if isInBlackList {
			return c.JSON(http.StatusNonAuthoritativeInfo, response.NewCusRes("this account is logged out"))
		}

		// verify the token
		verifiedToken, err := auth.VerifyToken(token)
		if err != nil {
			return c.JSON(http.StatusNonAuthoritativeInfo, response.NewCusRes(fmt.Sprintf("invalid token: %v", err.Error())))
		}
		c.Set("email", verifiedToken.Email)
		c.Set("permissions", verifiedToken.Permisions)
		return next(c)
	}
}
