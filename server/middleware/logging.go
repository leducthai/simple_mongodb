package middleware

import (
	"bytes"
	"io/ioutil"

	"github.com/labstack/echo"
	"go.uber.org/zap"
)

func LogMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Read the request body
		body, err := ioutil.ReadAll(c.Request().Body)
		if err != nil {
			return err
		}

		logger, _ := zap.NewProduction()
		loggers := []zap.Field{
			zap.String("Method", c.Request().Method),
			zap.String("Path", c.Request().URL.Path),
			zap.String("Body", string(body)),
		}
		// Log the HTTP method and path
		logger.Info("request", loggers...)

		// Restore the request body for subsequent use
		c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(body))

		// Call the next handler in the chain
		return next(c)
	}
}
