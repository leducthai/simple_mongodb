package routing

import (
	"github.com/labstack/echo"
	user "gitlab.com/leducthai/simple_mongodb/server/handler/users"
	"gitlab.com/leducthai/simple_mongodb/services/api"
)

func UserGroup(eg *echo.Group, sv api.ServicesInter) {
	{
		eg.GET("", user.GetUserHandle(sv))

		eg.POST("", user.CreateUserHandle(sv))

		eg.DELETE("", user.DeleteUserHandle(sv))

		eg.PUT("", user.UpdateUserHandle(sv))
	}
}
