package routing

import (
	"github.com/labstack/echo"
	"gitlab.com/leducthai/simple_mongodb/server/handler/auth"
	"gitlab.com/leducthai/simple_mongodb/server/middleware"
	"gitlab.com/leducthai/simple_mongodb/services/api"
)

func AuthGroup(eg *echo.Group, sv api.ServicesInter) {
	{
		eg.POST("/login", auth.LoginHandle(sv))

		eg.POST("/logout", auth.LogoutHandle(sv), middleware.Authmidleware)

		eg.POST("/refresh", auth.RefreshTokenHandle(sv), middleware.Authmidleware)

		eg.POST("", auth.CreateAccountHandle(sv), middleware.Authmidleware)
	}
}
