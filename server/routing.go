package server

import (
	"fmt"

	"github.com/labstack/echo"
	"gitlab.com/leducthai/simple_mongodb/server/middleware"
	"gitlab.com/leducthai/simple_mongodb/server/routing"
	"gitlab.com/leducthai/simple_mongodb/services/api"
)

const apiEndpoint = "/api"

func GroupingUrls(e *echo.Echo, sv api.ServicesInter) {
	routing.UserGroup(e.Group(fmt.Sprintf("%v/users", apiEndpoint), middleware.Authmidleware), sv)
	routing.AuthGroup(e.Group(fmt.Sprintf("%v/accounts", apiEndpoint)), sv)
}
