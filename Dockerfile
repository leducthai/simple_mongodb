FROM golang:1.20.6-alpine3.18 as server

# add necessary features (also for ci/cd)
RUN apk update && apk upgrade
RUN apk add --no-cache git
RUN apk add --update --no-cache curl build-base
RUN apk update && apk add openssh

ENV SERVER_DIR ${GOPATH}/src/gitlab.com/leducthai/simple_mongodb/

COPY . ${SERVER_DIR}

WORKDIR ${SERVER_DIR}

RUN go mod download
RUN go vet ./...
# (no database connecttion for test)
# RUN go test ./... 
# RUN go build -race -ldflags "-extldflags '-static'" -o /opt/simple-mongo/server .

CMD ["/bin/sh"]

# FROM alpine:latest

# WORKDIR /root/

# COPY --from=server /opt/simple-mongo/server /root/server
